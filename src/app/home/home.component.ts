import { Component } from '@angular/core';
import { LoginService } from '../../app/loginService';
import { ActivatedRoute, Router } from '@angular/router';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
    providers: [LoginService],
})
export class HomeComponent {
  title = 'app works!';
     idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
   constructor(
        private router:Router,private route: ActivatedRoute,
        private loginService: LoginService,private idle: Idle, private keepalive: Keepalive) {
             idle.setIdle(5);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(5);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
    idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      localStorage.removeItem("AccessToken");
        this.router.navigate(['home'])
    });
    idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
    idle.onTimeoutWarning.subscribe((countdown) => this.idleState = 'You will time out in ' + countdown + ' seconds!');

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => this.lastPing = new Date());

    this.reset();
    }

    localLogin(){
      //  loca  localRegister() {
       /*   this.loginService.registerApplicationUser('gkoolu@e2etek.com','Test!234','555555555')
           .subscribe(
            data=>{
                console.log('registerApplicationUser');
            },
               error=>{
                var err = error.json().message;
                 console.log(err);
            })    */  
            //  this.router.navigate(['/test'])
      
 localStorage.setItem("AccessToken","TestToken");
   this.router.navigate(['test'])
          this.loginService.CheckToken()
           .subscribe(
            data=>{
                console.log('CheckToken');
                console.log(data);
                this.router.navigate(['test'])
            },
               error=>{
                var err = error.json().message;
                 console.log(err);
            }) 
    }

  

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }
//location.href="http://localhost:53593/api/accounts/ExternalLogin?provider=Facebook&response_type=token&client_id=188072818396948&redirect_uri=http://localhost:4200/aboutus";

    }