import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {

    dato: Array<any>;

    constructor(private http: Http) { }

    getUsers() {
        /* return this.http.get('assets/users.json')
              .map(res => res.json());*/

    }

    registerApplicationUser(email, password, pinno) {

        var user = JSON.stringify({
            Email: email,
            Password: password,
            /*                         TenantId: UUID.UUID() + username, */
            Pin: pinno
        })
        console.log(user);

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost:53593/api/accounts/create', user, options)
            .map(res => res.json());

    }

    registerSocialUser(username, firstname, lastname, provider, ExternalAccessToken, pin) {
        if (pin == "") pin = 0;
        var user = JSON.stringify({
            UserName: username,
            FirstName: "",
            LastName: "",
            Provider: provider,
            PIN: pin,
            ExternalAccessToken: ExternalAccessToken
        })
        console.log(user);

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost:53593/api/accounts/RegisterExternal', user, options)
            .map(res => res.json());

    }
    /*   localUserLogin(username,password){
       var headers = new Headers();
       headers.append('Content-Type', 'application/x-www-form-urlencoded');
       headers.append('Access-Control-Allow-Headers', 'Content-Type');
       headers.append('Access-Control-Allow-Methods', 'GET');
       headers.append('Access-Control-Allow-Origin', '*');
       var creds = "username=" + username + "&password=" + password;
       this.http.post('http://localhost:53593/oauth/token', creds, {
           headers: headers
           })
           .map(res => res.json())
           .subscribe(
             data => console.log(data),
             err => console.log(err),
             () => console.log('Authentication Complete')
           );
      } */

    CheckToken() {
        let headers = new Headers({
            'Accept': 'application/json'
        });
        headers.append('Content-Type', 'application/json');

        headers.append("Authorization", "Bearer yrsFtnQxws-WGOnm9fCj0UUAt05xepaFRReEPqvsnOStX8V97Mj22geWlv4hPalai5KMbOlKko0LIYhG7T3hHLsvfe6hBWhsaizLtQbsZfHg7kUF_rEUlNufCAUB1ts6qO3dHQE1HzIcJODzMstrOrbib7rEMpcUtQ__Mo2QHSz8bJnrTIsKzPYkxZ0UiTIUfz_DA_uaAgJy3rCMTSSAqrbBhvMXyXWJ05Po0nh9RP2ymsQ5h7KoabhW9hgK0pw5QcM-uu4vAsWsgifVWLC-rcmVQA3k1eN7YpbDAV7l0DUdgUCQJcyXV3AYldDCDLWISRUdRZD3b0GtYb3bymHA1l_5AFtV4qMWaK_yu8nC2C4_oYb6YQu8tyrjsbbXubx2eO3fZsPc7m5BcZTqekLYxU4ah5VP0bXIXYPebV-drSerZGVJ5PemGz-JP3SOr_g03F3sEHebcq2bZjuCd1TdtrHx0Ou0fQTvJzsdjHz-fpQ");
        let requestOptions = new RequestOptions({ headers: headers });
        return this.http.get('http://localhost:53593/api/accounts/checktoken', requestOptions)
            .map(res => res.json());

    }

    urlEncode(obj: Object): string {
        let urlSearchParams = new URLSearchParams();
        for (let key in obj) {
            urlSearchParams.append(key, obj[key]);
        }
        return urlSearchParams.toString();
    }
    loginLocalUser() {
           let headers = new Headers({
            'Accept': 'application/x-www-form-urlencoded'
        });
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        headers.append("Authorization", "Bearer KDzF_ETXwwuRWA3irQLSAkMoFMYk0UkOygoy-M1vxYXsIAvdhJ2FD6LLZpYD_OSmETp1MApcObZcsiQtGREoDILiai2wSGGQ5qkCHmJMghKbn2O9E5mXO912ZNfutN_y86fLkwYKk1K_bpP-MRElHFIEDEz516m_i8x8h3Dy7CoHJ0butt4F1FyUxzwmMX7nvhbnjqIA3u4bKLW7sNrn-yMUqwlrucCOGt3k0F7_u-qjx5Zn4Fp5zHwsVz1lUuQgZvNpCWpVJV_XCu1UPw9JUW2VijOAi_JxvoTjkdjmY7B_5dLH_gAfL0irTvUSshyhr-ASFEfARHxkzW4YkYFvRVRXjglg_x07RLLQuIwlpbyF97qRS7-NiPCM0r0Xq23Wt9S2483q42vIlg7crujb54iJmqs5L09mIK4-sJS8cyxeHfD7Nyj5H3XsQjiN2xki5vBTr-C77rjUhPezjbMuc82MGWS0JUPF7kelhMl3sus");

           var details = {
                'userName': 'gkoolu@e2etek.com',
                'password': 'Test!234',
                'grant_type': 'Test!234'
            };

        let requestOptions = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost:53593/oauth/token',details, requestOptions)
            .map(res => res.json());
       /*  var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('email', username);
        urlSearchParams.append('password', password);
        urlSearchParams.append('grant_type','password');
        let body = urlSearchParams.toString()
    
        return this.http.post('http://localhost:53593/oauth/token', body, {headers: headers})
            .map((response: Response) => {
                console.log(response);
                // login successful if user.status = success in the response
                let user = response.json();
                console.log(user.status)
                if (user && "success" == user.status) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                }
            }); */

/* 
            var details = {
                'userName': 'test@gmail.com',
                'password': 'Password!',
                'grant_type': 'password'
            };
            
            var formBody = [];
            for (var property in details) {
              var encodedKey = encodeURIComponent(property);
              var encodedValue = encodeURIComponent(details[property]);
              formBody.push(encodedKey + "=" + encodedValue);
            }
            formBody.join("&");
            let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
            headers.append("Accept","application/json");
            let requestOptions = new RequestOptions({ headers: headers });
            return this.http.post('http://localhost:53593/oauth/token',formBody.toString(),requestOptions)
                    .map((response: Response) => {
    
                    let token = response.json();
                    console.log(token);
                    if (token) {
    
                        console.log(token);
    
                    } 
                }) */
          
     
     /*    console.log(username);
        console.log(password);

        let body = new URLSearchParams();
        body.set('username', username);
        body.set('password', password);
        body.set('grant_type', 'password');
        console.log(this.urlEncode(body));
        
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let requestOptions = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost:53593/oauth/token',body.toString(),requestOptions)
                .map((response: Response) => {

                let token = response.json();
                console.log(token);
                if (token) {

                    console.log(token);

                } */
                /*  let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
                  return this.http.post('http://localhost:53593/oauth/token', {'username': username, 'password':  password, 'grant_type':'password'}, headers)
     
                 .map((response: Response) => {
     
                     let token = response.json();
     
                     if (token) {
     
                        console.log(token);
     
                     }*/

        

    }

}