import { TestangularcliPage } from './app.po';

describe('testangularcli App', () => {
  let page: TestangularcliPage;

  beforeEach(() => {
    page = new TestangularcliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
